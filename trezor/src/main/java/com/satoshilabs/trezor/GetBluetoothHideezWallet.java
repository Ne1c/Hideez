package com.satoshilabs.trezor;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.satoshilabs.trezor.protobuf.TrezorMessage.Address;
import com.satoshilabs.trezor.protobuf.TrezorMessage.ButtonRequest;
import com.satoshilabs.trezor.protobuf.TrezorMessage.Entropy;
import com.satoshilabs.trezor.protobuf.TrezorMessage.EntropyRequest;
import com.satoshilabs.trezor.protobuf.TrezorMessage.Failure;
import com.satoshilabs.trezor.protobuf.TrezorMessage.Features;
import com.satoshilabs.trezor.protobuf.TrezorMessage.MessageSignature;
import com.satoshilabs.trezor.protobuf.TrezorMessage.MessageType;
import com.satoshilabs.trezor.protobuf.TrezorMessage.PassphraseRequest;
import com.satoshilabs.trezor.protobuf.TrezorMessage.PinMatrixRequest;
import com.satoshilabs.trezor.protobuf.TrezorMessage.PublicKey;
import com.satoshilabs.trezor.protobuf.TrezorMessage.Success;
import com.satoshilabs.trezor.protobuf.TrezorMessage.TxRequest;
import com.satoshilabs.trezor.protobuf.TrezorMessage.TxSize;
import com.satoshilabs.trezor.protobuf.TrezorMessage.WordRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

public class GetBluetoothHideezWallet {
    public static final String TAG = "hidezz_bluetooth";

    @SuppressLint("MissingPermission")
    public BluetoothDevice get() {
        BluetoothDevice[] devices = BluetoothAdapter.getDefaultAdapter()
                .getBondedDevices()
                .toArray(new BluetoothDevice[]{});

        for (BluetoothDevice device : devices) {
            if (device.getName().equalsIgnoreCase("hideezwallet")) {
                return device;
            }
        }

        return null;
    }

    public static class AcceptThread extends Thread {
        private BluetoothDevice mBluetoothDevice;
        private OutputStream outputStream;
        private InputStream inStream;

        public AcceptThread(BluetoothDevice bluetoothDevice) {
            mBluetoothDevice = bluetoothDevice;
        }

        @Override
        public synchronized void start() {
            super.start();

            BluetoothSocket socket = openStream();

            try {
                socket.connect();
                outputStream = socket.getOutputStream();
                inStream = socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @SuppressLint("MissingPermission")
        private BluetoothSocket openStream() {

            BluetoothSocket socket = null;
            try {

                UUID uuid = mBluetoothDevice.getUuids()[0].getUuid();
                // CREATE SOCKET
                try {
                    socket = mBluetoothDevice.createRfcommSocketToServiceRecord(uuid);
                } catch (Exception e) {
                    socket = null;
                }

                if (null == socket) {
                    try {
                        Method m = mBluetoothDevice.getClass().getMethod("createRfcommSocket", int.class);
                        socket = (BluetoothSocket) m.invoke(mBluetoothDevice, 1);
                    } catch (Exception e) {
                        socket = null;
                    }
                }

                if (null == socket) {
                    try {
                        socket = mBluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);
                    } catch (Exception e) {
                        socket = null;
                    }
                }

//                        = (BluetoothSocket) mBluetoothDevice.getClass()
//                        .getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class })
//                        .invoke(mBluetoothDevice, mBluetoothDevice.getUuids()[0].getUuid());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return socket;
        }

        private void messageWrite(Message msg) {
            int msg_size = msg.getSerializedSize();
            String msg_name = msg.getClass().getSimpleName();
            int msg_id = MessageType.valueOf("MessageType_" + msg_name).getNumber();
            Log.d(TAG, String.format("Got message: %s", msg_name));
            ByteBuffer data = ByteBuffer.allocate(32768);
            data.put((byte) '#');
            data.put((byte) '#');
            data.put((byte) ((msg_id >> 8) & 0xFF));
            data.put((byte) (msg_id & 0xFF));
            data.put((byte) ((msg_size >> 24) & 0xFF));
            data.put((byte) ((msg_size >> 16) & 0xFF));
            data.put((byte) ((msg_size >> 8) & 0xFF));
            data.put((byte) (msg_size & 0xFF));
            data.put(msg.toByteArray());
            while (data.position() % 63 > 0) {
                data.put((byte) 0);
            }

            int chunks = data.position() / 15;
            Log.d(TAG, String.format("Writing %d chunks", chunks));
            data.rewind();
            for (int i = 0; i < chunks; i++) {
                byte[] buffer = new byte[16];
                buffer[0] = (byte) '?';
                data.get(buffer, 1, 15);
            /*
			String s = "chunk:";
			for (int j = 0; j < 64; j++) {
				s += String.format(" %02x", buffer[j]);
			}
			Log.i("Trezor.messageWrite()", s);
			*/
                try {
                    outputStream.write(ByteBuffer.wrap(buffer).array());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private Message parseMessageFromBytes(MessageType type, byte[] data) {
            Message msg = null;
            Log.i(TAG, String.format("Parsing %s (%d bytes):", type, data.length));
            try {
                if (type.getNumber() == MessageType.MessageType_Success_VALUE) {
                    msg = Success.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_Failure_VALUE) {
                    msg = Failure.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_Entropy_VALUE) {
                    msg = Entropy.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_PublicKey_VALUE) {
                    msg = PublicKey.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_Features_VALUE) {
                    msg = Features.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_PinMatrixRequest_VALUE) {
                    msg = PinMatrixRequest.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_TxRequest_VALUE) {
                    msg = TxRequest.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_ButtonRequest_VALUE) {
                    msg = ButtonRequest.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_Address_VALUE) {
                    msg = Address.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_EntropyRequest_VALUE) {
                    msg = EntropyRequest.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_MessageSignature_VALUE) {
                    msg = MessageSignature.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_PassphraseRequest_VALUE) {
                    msg = PassphraseRequest.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_TxSize_VALUE) {
                    msg = TxSize.parseFrom(data);
                }
                if (type.getNumber() == MessageType.MessageType_WordRequest_VALUE) {
                    msg = WordRequest.parseFrom(data);
                }
            } catch (InvalidProtocolBufferException e) {
                Log.e(TAG, e.toString());
                return null;
            }
            return msg;
        }

        private Message messageRead() {
            final int BUFFER_SIZE = 1024;
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytes = 0;
            int b = BUFFER_SIZE;

            while (bytes != BUFFER_SIZE) {
                try {
                    bytes = inStream.read(buffer, bytes, BUFFER_SIZE - bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

//        ByteBuffer data = ByteBuffer.allocate(32768);
//        ByteBuffer buffer = ByteBuffer.allocate(16);
//        UsbRequest request = new UsbRequest();
//        request.initialize(conn, epr);
//        MessageType type;
//        int msg_size;
//        int repeats = 0;
//        for (; ; ) {
//            request.queue(buffer, 16);
//            conn.requestWait();
//            byte[] b = buffer.array();
//            Log.d(TAG, String.format("Read chunk: %d bytes", b.length));
//
//            if (repeats > 100) {
//                // Sometimes we get infinite empty packets - break after 100
//                Log.e(TAG, "Read aborted after 100 packets.");
//                throw new ExtSigDeviceConnectionException("Unable to read response from ExtSigDevice");
//            }
//            repeats++;
//
//            if (b.length < 9) {
//                continue;
//            }
//            if (b[0] != (byte) '?' || b[1] != (byte) '#' || b[2] != (byte) '#') {
//                continue;
//            }
//            type = MessageType.valueOf((b[3] << 8) + b[4]);
//            if (b[0] != (byte) '?') {
//                continue;
//            }
//
//            // Cast to "unsigned Byte" and combine to int
//            msg_size = (
//                    ((int) b[5] & 0x8F) << (8 * 3)) +
//                    (((int) b[6] & 0xFF) << (8 * 2)) +
//                    (((int) b[7] & 0xFF) << 8) +
//                    ((int) b[8] & 0xFF);
//
//            data.put(b, 9, b.length - 9);
//            break;
//        }
//        while (data.position() < msg_size) {
//            request.queue(buffer, 64);
//            conn.requestWait();
//            byte[] b = buffer.array();
//            Log.d(TAG, String.format("Read chunk (cont): %d bytes", b.length));
//            data.put(b, 1, b.length - 1);
//        }
//        request.close();

            MessageType type = MessageType.valueOf((buffer[3] << 8) + buffer[4]);

            // Cast to "unsigned Byte" and combine to int
//        msg_size = (
//                ((int) b[5] & 0x8F) << (8 * 3)) +
//                (((int) b[6] & 0xFF) << (8 * 2)) +
//                (((int) b[7] & 0xFF) << 8) +
//                ((int) b[8] & 0xFF);
//
//        data.put(b, 9, b.length - 9);
            return parseMessageFromBytes(type, Arrays.copyOfRange(buffer, 0, BUFFER_SIZE));
        }

        public Message send(Message msg) {
            messageWrite(msg);
            return messageRead();
        }
    }

    public static class AcceptThread2 extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        private BluetoothDevice mBluetoothDevice;

        public AcceptThread2(BluetoothDevice bluetoothDevice) {
            mBluetoothDevice = bluetoothDevice;

            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = BluetoothAdapter.getDefaultAdapter()
                        .listenUsingRfcommWithServiceRecord(mBluetoothDevice.getName(),
                                mBluetoothDevice.getUuids()[0].getUuid());
            } catch (IOException e) {
                Log.e(TAG, "Socket's listen() method failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "Socket's accept() method failed", e);
                    break;
                }

                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
//                    manageMyConnectedSocket(socket);
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }
}
