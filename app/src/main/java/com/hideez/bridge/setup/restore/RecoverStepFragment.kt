package com.hideez.bridge.setup.restore


import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import kotlinx.android.synthetic.main.fragment_recover_step.*


class RecoverStepFragment : Fragment() {
    private var steps = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recover_step, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        wordEditText.setTypeface(Typeface.DEFAULT)
        wordEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                nextButton.isEnabled = s.toString().isNotEmpty()
                nextButton.alpha = if (nextButton.isEnabled) 1f else 0.5f
            }

        })

        nextButton.setOnClickListener {
            if (steps == 24) {
                activity?.supportFragmentManager
                        ?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, RestoreSuccessFragment())
                        ?.commitNowAllowingStateLoss()
            } else {
                (activity as RestoreActivity).sendWord(steps, wordEditText.text.toString())
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun successSendWord() {
        steps++

        stepTextView.text = "$steps/24"
        wordEditText.setText("")

        if (steps == 24) {
            nextButton.text = "Confirm"
        }
    }
}
