package com.hideez.bridge.setup.pin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import com.hideez.io.command.CommandListener
import com.hideez.io.model.hideez.SetPinDataModel

class PinSetupActivity : AppCompatActivity() {
    private val listener = object : CommandListener() {
        override fun setPin(stage: SetPinDataModel.Stage) {
            when (stage) {
                SetPinDataModel.Stage.ENTER -> {
                    supportFragmentManager?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, EnterPinFragment())
                            ?.commitAllowingStateLoss()
                }
                SetPinDataModel.Stage.REPEAT -> {
                    supportFragmentManager?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, RepeatPinFragment())
                            ?.commitAllowingStateLoss()
                }
                SetPinDataModel.Stage.CONFIRM -> {
                    supportFragmentManager?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, ConfirmPinFragment())
                            ?.commitAllowingStateLoss()
                }
            }
        }

        override fun pinSet() {
            supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.fragmentContainer, SuccessSetupPinFragment())
                    ?.commitAllowingStateLoss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_setup)

        supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, HowToSetupPinFragment.newInstance())
                ?.commitAllowingStateLoss()

        HideezIO.INSTANCE.addListener(listener)
    }

    override fun onDestroy() {
        HideezIO.INSTANCE.removeListener(listener)
        super.onDestroy()
    }
}

