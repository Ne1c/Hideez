package com.hideez.bridge.setup.restore

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import com.hideez.io.command.CommandListener
import com.hideez.io.command.Errors
import com.hideez.io.model.hideez.ErrorModel

class RestoreActivity : AppCompatActivity() {
    private val listener = object : CommandListener() {
        override fun recoverWordWasSend() {
            (supportFragmentManager
                    .findFragmentById(R.id.fragmentContainer) as RecoverStepFragment).successSendWord()
        }

        override fun error(value: ErrorModel) {
            if (value.msgId == Errors.ERR_BAD_RECOVERY_DATA) {
                supportFragmentManager?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, RestoreErrorFragment())
                        ?.commitAllowingStateLoss()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restore)

        HideezIO.INSTANCE.addListener(listener)

        supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, RestoreIntroFragment())
                ?.commitAllowingStateLoss()
    }

    override fun onDestroy() {
        super.onDestroy()

        HideezIO.INSTANCE.removeListener(listener)
    }

    fun finishRecover() {
        HideezIO.INSTANCE.bridgeApi.activate()
    }

    fun sendWord(number: Int, word: String) {
        HideezIO.INSTANCE.bridgeApi.recover(number, word)
    }
}
