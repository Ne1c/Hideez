package com.hideez.bridge.setup.pin


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import kotlinx.android.synthetic.main.fragment_how_to_setup_pin.*

class HowToSetupPinFragment : Fragment() {
    companion object {
        fun newInstance(): HowToSetupPinFragment {
            return HowToSetupPinFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_how_to_setup_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nextButton.setOnClickListener {
            HideezIO.INSTANCE.bridgeApi.init()
        }
    }
}
