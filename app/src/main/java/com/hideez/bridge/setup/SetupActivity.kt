package com.hideez.bridge.setup

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hideez.bridge.R
import com.hideez.bridge.setup.pin.PinSetupActivity
import com.hideez.bridge.setup.restore.RestoreActivity
import kotlinx.android.synthetic.main.activity_setup.*

class SetupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        setupButton.setOnClickListener {
            startActivity(Intent(this, PinSetupActivity::class.java))
            finish()
        }

        recoverButton.setOnClickListener {
            startActivity(Intent(this, RestoreActivity::class.java))
            finish()
        }

    }
}
