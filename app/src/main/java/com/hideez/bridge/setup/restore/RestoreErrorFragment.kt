package com.hideez.bridge.setup.restore


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hideez.bridge.R
import com.hideez.bridge.backup.BackupActivity
import kotlinx.android.synthetic.main.fragment_restore_error.*

class RestoreErrorFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restore_error, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startAgainButton.setOnClickListener {
            activity?.recreate()
        }

        getNewWalletButton.setOnClickListener {
            activity?.finish()

            startActivity(Intent(activity, BackupActivity::class.java))
        }
    }
}
