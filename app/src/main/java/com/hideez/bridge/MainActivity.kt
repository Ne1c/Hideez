package com.hideez.bridge

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.hideez.bridge.intro.IntroActivity
import com.hideez.io.HideezIO
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
//    override fun backupDone() {
//        Toast.makeText(this, "Backup done", Toast.LENGTH_SHORT).show()
//    }
//
//    override fun auth() {
//        Toast.makeText(this, "Auth reply", Toast.LENGTH_SHORT).show()
//    }
//
//    override fun queryPin() {
//        Toast.makeText(this, "Query pin", Toast.LENGTH_SHORT).show()
//    }
//
//    override fun setPin(stage: SetPinDataModel.Stage) {
//        Toast.makeText(this, stage.toString(), Toast.LENGTH_SHORT).show()
//    }
//
//    override fun pinSet() {
//        Toast.makeText(this, "Pin set", Toast.LENGTH_SHORT).show()
//
//        bleManager.bridgeApi.requestDeviceInfo()
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        PreferenceKeyStore.init(PreferenceManager.getDefaultSharedPreferences(applicationContext))

        HideezIO.init(this)

        val bleManager = HideezIO.INSTANCE
        val device = getDevice()
        connectButton.setOnClickListener {
            if (device == null) {
                Toast.makeText(this, "Bluetooth should be enabled and paired with device", Toast.LENGTH_LONG).show()
            } else {
                bleManager.connect(getDevice()!!)
            }
        }

        authButton.setOnClickListener {
            if (!bleManager.isConnected) {
                bleManager.connect(getDevice()!!)
            } else {
                bleManager.bridgeApi.auth()
            }
        }

        initButton.setOnClickListener {
            if (!bleManager.isConnected) {
                bleManager.connect(getDevice()!!)
            } else {
                bleManager.bridgeApi.init()
            }
        }

        getInfoButton.setOnClickListener {
            if (!bleManager.isConnected) {
                bleManager.connect(getDevice()!!)
            } else {
                bleManager.bridgeApi.requestDeviceInfo()
            }
        }

        backupButton.setOnClickListener {
            if (!bleManager.isConnected) {
                bleManager.connect(getDevice()!!)
            } else {
                bleManager.bridgeApi.backup()
            }
        }

        var i = 0
        recoveryButton.setOnClickListener {
            if (!bleManager.isConnected) {
                bleManager.connect(getDevice()!!)
            } else {
                if (i == 24) {
                    bleManager.bridgeApi.activate()
                } else {
                    i++
                    bleManager.bridgeApi.recover(i, recoverEditText.text.toString())
                }
            }
        }

        wipeButton.setOnClickListener {
            bleManager.bridgeApi.wipe()
        }

        startActivity(Intent(this, IntroActivity::class.java))
    }
    
    fun getDevice() : BluetoothDevice? {
        return BluetoothAdapter.getDefaultAdapter().bondedDevices.find {
            it.name.contains("Hideez")
        }
    }
}
