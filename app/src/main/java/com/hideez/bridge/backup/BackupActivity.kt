package com.hideez.bridge.backup

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import com.hideez.io.command.CommandListener
import java.util.*

class BackupActivity : AppCompatActivity() {
    private val listener = object : CommandListener() {
        override fun backupWord(number: Int, word: String) {
            backupWordFragment.setWord(number, word)
            words.add(word)

            Log.d("BackupActivity", "number: $number; word: $word")
        }
    }

    private var backupWordFragment: BackupWordFragment = BackupWordFragment()
    private val words = LinkedList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backup)

        supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, BackupIntroFragment())
                ?.commitAllowingStateLoss()

        HideezIO.INSTANCE.addListener(listener)
    }

    override fun onDestroy() {
        HideezIO.INSTANCE.removeListener(listener)

        super.onDestroy()
    }

    fun startReadWords() {
        supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, backupWordFragment)
                ?.commitAllowingStateLoss()

        HideezIO.INSTANCE.bridgeApi.backup()
    }

    fun startConfirmWords() {
        supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragmentContainer, BackupConfirmFragment.newInstance(words))
                ?.commitAllowingStateLoss()
    }

    fun activate() {
        HideezIO.INSTANCE.bridgeApi.activate()
    }
}
