package com.hideez.bridge.backup


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import kotlinx.android.synthetic.main.fragment_backup_confirm.*
import java.util.*

class BackupConfirmFragment : Fragment() {
    companion object {
        fun newInstance(words: LinkedList<String>): BackupConfirmFragment {
            return BackupConfirmFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("words", words)
                }
            }
        }
    }

    private lateinit var words: LinkedList<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_backup_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        words = arguments?.getSerializable("words") as LinkedList<String>


        generateViewContent(-1)

    }

    @SuppressLint("SetTextI18n")
    private fun generateViewContent(previousNumber: Int) {
        val indexes = generateAndSetWords(previousNumber)

        numberOfWordTextView.text = "Choose your word number ${indexes.second + 1}"
        button1.setOnClickListener(generateOnClickListener(previousNumber, 0, indexes.first))
        button2.setOnClickListener(generateOnClickListener(previousNumber, 1, indexes.first))
        button3.setOnClickListener(generateOnClickListener(previousNumber, 2, indexes.first))
        button4.setOnClickListener(generateOnClickListener(previousNumber, 3, indexes.first))
    }

    private fun generateOnClickListener(previousNumber: Int, buttonIndex: Int,
                                        correctButtonIndex:Int) : View.OnClickListener {
        return View.OnClickListener {
            if (previousNumber == -1 && correctButtonIndex == buttonIndex) {
                generateViewContent(correctButtonIndex)
            } else if (previousNumber != -1 && correctButtonIndex == buttonIndex) {
                (activity as BackupActivity).activate()
                activity?.supportFragmentManager
                        ?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, BackupSuccessFragment())
                        ?.commitAllowingStateLoss()
            } else {
                activity?.supportFragmentManager
                        ?.beginTransaction()
                        ?.replace(R.id.fragmentContainer, BackupErrorFragment())
                        ?.commitAllowingStateLoss()
            }
        }
    }

    // return - first is number of correct button, second is index of word should pick as right
    private fun generateAndSetWords(previousNumber: Int): Pair<Int, Int> {
        val pair = Pair(Random().nextInt(4), getIndexOfCorrectWord(previousNumber))

        when (pair.first) {
            0 -> button1.text = words[pair.second]
            1 -> button2.text = words[pair.second]
            2 -> button3.text = words[pair.second]
            3 -> button4.text = words[pair.second]
        }

        val randomNumbers = generateWrongWords(pair.second)

        when (pair.first) {
            0 -> {
                button2.text = words[randomNumbers[0]]
                button3.text = words[randomNumbers[1]]
                button4.text = words[randomNumbers[2]]
            }
            1 -> {
                button1.text = words[randomNumbers[0]]
                button3.text = words[randomNumbers[1]]
                button4.text = words[randomNumbers[2]]
            }
            2 -> {
                button1.text = words[randomNumbers[0]]
                button2.text = words[randomNumbers[1]]
                button4.text = words[randomNumbers[2]]
            }
            3 -> {
                button1.text = words[randomNumbers[0]]
                button2.text = words[randomNumbers[1]]
                button3.text = words[randomNumbers[2]]
            }
        }

        return pair
    }

    private fun getIndexOfCorrectWord(previousNumber: Int): Int {
        val randomIndex = Random().nextInt(words.size)

        return if (randomIndex == previousNumber) getIndexOfCorrectWord(previousNumber)
        else randomIndex
    }

    private fun generateWrongWords(except: Int): Array<Int> {
        val array = mutableSetOf<Int>()
        val random = Random()

        while (array.size != 3) {
            val element = random.nextInt(words.size)

            if (element != except) array.add(element)
        }

        return array.toTypedArray()
    }
}
