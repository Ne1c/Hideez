package com.hideez.bridge.backup

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import kotlinx.android.synthetic.main.fragment_backup_word.*
import kotlinx.android.synthetic.main.fragment_backup_word.view.*

class BackupWordFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_backup_word, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        learnHowButton.setOnClickListener {
            AlertDialog.Builder(activity)
                    .setView(R.layout.dialog_use_other_device_words)
                    .create()
                    .show()
        }
    }

    @SuppressLint("SetTextI18n")
    fun setWord(number: Int, word: String) {
        view?.stepTextView?.text = "$number/24"
        view?.wordTextView?.text = word

        if (number == 12) {
            switchDeviceInfo.visibility = View.VISIBLE
            learnHowButton.visibility = View.VISIBLE
            pressButtonOnDeviceImageView.visibility = View.GONE
            pressTipTextView.visibility = View.GONE
            spin_kit.visibility = View.GONE
        } else {
            switchDeviceInfo.visibility = View.GONE
            learnHowButton.visibility = View.GONE
            pressButtonOnDeviceImageView.visibility = View.VISIBLE
            pressTipTextView.visibility = View.VISIBLE
            spin_kit.visibility = View.VISIBLE
        }

        if (number == 24) {
            learnHowButton.text = "Confirm"
            learnHowButton.setOnClickListener {
                (activity as BackupActivity).startConfirmWords()
            }
            learnHowButton.visibility = View.VISIBLE
            pressButtonOnDeviceImageView.visibility = View.GONE
            pressTipTextView.visibility = View.GONE
            spin_kit.visibility = View.GONE
        }
    }
}
