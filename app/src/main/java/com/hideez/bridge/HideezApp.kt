package com.hideez.bridge

import android.bluetooth.BluetoothDevice
import android.support.multidex.MultiDexApplication
import android.widget.Toast
import com.hideez.io.AbsHideezGattCallback
import com.hideez.io.HideezIO

class HideezApp : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        HideezIO.init(this)

        HideezIO.INSTANCE.addGattCallback(object : AbsHideezGattCallback() {
            override fun onDeviceDisconnected(device: BluetoothDevice?) {
                Toast.makeText(applicationContext, "Device disconnected", Toast.LENGTH_SHORT).show()
            }
        })
    }
}