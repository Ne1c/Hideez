package com.hideez.bridge.connect


import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import kotlinx.android.synthetic.main.fragment_pairing.view.*


class PairingFragment : Fragment() {
    companion object {
        fun newInstance(device: BluetoothDevice): PairingFragment {
            return PairingFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("device", device)
                }
            }
        }
    }

    private var bondingDevice: BluetoothDevice? = null

    private val bondReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) {
                val state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR)
                when (state) {
                    BluetoothDevice.BOND_BONDED -> {
                        HideezIO.INSTANCE.connect(bondingDevice!!)
                        activity?.supportFragmentManager
                                ?.beginTransaction()
                                ?.replace(R.id.fragmentContainer, PairedSuccessFragment.newInstance(bondingDevice!!.name))
                                ?.commitAllowingStateLoss()
                    }
                    BluetoothDevice.BOND_BONDING -> {

                    }
                    BluetoothDevice.BOND_NONE -> {
                        bondingDevice = null
                        activity?.supportFragmentManager
                                ?.beginTransaction()
                                ?.replace(R.id.fragmentContainer, ErrorConnectionFragment.newInstance(ErrorConnectionFragment.Error.PAIRING_FAILED))
                                ?.commitAllowingStateLoss()
                    }
                }
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pairing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bondingDevice = arguments!!.getParcelable<BluetoothDevice>("device")
        view.titleTextView.text = "Pairing with the \n${bondingDevice!!.name}"

        activity?.registerReceiver(bondReceiver, IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED))
        bondingDevice?.createBond()
    }

    override fun onDestroyView() {
        activity?.unregisterReceiver(bondReceiver)
        super.onDestroyView()
    }
}
