package com.hideez.bridge.connect


import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R

class SearchFragment : Fragment() {
    private val LOCATION_REQUEST_CODE = 555

    companion object {
        fun newInstance(): Fragment {
            return SearchFragment()
        }
    }

    private val devicesList = arrayListOf<BluetoothDevice>()
    private val bluetoothAdapter: BluetoothLeScanner by lazy {
        BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
    }

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)

            if (result?.device?.bondState == BluetoothDevice.BOND_BONDED) {
                Log.d("Wow", "Wow")
            }

            activity?.runOnUiThread {
                if (!devicesList.contains(result?.device) &&
                        result?.device != null && !result.device.name.isNullOrEmpty() &&
                        result.device?.name!!.contains("Hideez")) {
                    devicesList.add(result.device)
                }
            }
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val defaultAdapter = BluetoothAdapter.getDefaultAdapter()

        if (!defaultAdapter.isEnabled) {
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.fragmentContainer,
                            ErrorConnectionFragment.newInstance(ErrorConnectionFragment.Error.BLUETOOTH_DISABLE))
                    ?.commitAllowingStateLoss()
        } else if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
        } else {
            bluetoothAdapter.startScan(arrayListOf<ScanFilter>(), ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build(), scanCallback)

            Handler().postDelayed({
                bluetoothAdapter.stopScan(scanCallback)

                devicesList.addAll(defaultAdapter.bondedDevices.filter {
                    !devicesList.contains(it) && it.name.contains("Hideez")
                })

                if (devicesList.size > 0) {
                    activity?.supportFragmentManager?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, DeviceListFragment.newInstance(devicesList))
                            ?.commitAllowingStateLoss()
                } else {
                    activity?.supportFragmentManager?.beginTransaction()
                            ?.replace(R.id.fragmentContainer,
                                    ErrorConnectionFragment.newInstance(ErrorConnectionFragment.Error.DONT_SEARCH_HIDEEZ))
                            ?.commitAllowingStateLoss()
                }
            }, 10000)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == LOCATION_REQUEST_CODE && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
        } else {
            bluetoothAdapter.startScan(scanCallback)
        }
    }
}
