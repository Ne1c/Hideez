package com.hideez.bridge.connect

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.BOND_BONDED
import android.bluetooth.BluetoothDevice.BOND_NONE
import android.bluetooth.le.*
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import com.hideez.io.HideezIO
import kotlinx.android.synthetic.main.fragment_device_list.*
import kotlinx.android.synthetic.main.item_device_ble.view.*

class DeviceListFragment : Fragment() {
    companion object {
        const val LIST_DEVICES = "list_devices"

        fun newInstance(list: ArrayList<BluetoothDevice> = arrayListOf()): DeviceListFragment {
            return DeviceListFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(LIST_DEVICES, list)
                }
            }
        }
    }

    private val bluetoothAdapter: BluetoothLeScanner by lazy {
        BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
    }

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)

            activity?.runOnUiThread {
                (devicesRecyclerView.adapter as DeviceListAdapter).addDevice(result!!.device)
            }
        }
    }

    private fun scanLeDevice() {
        bluetoothAdapter.stopScan(scanCallback)

        bluetoothAdapter.startScan(arrayListOf<ScanFilter>(), ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build(), scanCallback)

        view?.postDelayed({
            BluetoothAdapter.getDefaultAdapter().bondedDevices.filter {
                it.name.contains("Hideez")
            }.forEach {
                (devicesRecyclerView.adapter as DeviceListAdapter).addDevice(it)
            }

            bluetoothAdapter.stopScan(scanCallback)
        }, 10000)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_device_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        devicesRecyclerView.layoutManager = LinearLayoutManager(context)
        val deviceListAdapter = DeviceListAdapter(object : DeviceListAdapter.Callback {
            override fun clickDevice(device: BluetoothDevice) {
                if (device.bondState == BOND_NONE) {
                    activity?.supportFragmentManager
                            ?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, PairingFragment.newInstance(device))
                            ?.commitAllowingStateLoss()
                } else if (device.bondState == BOND_BONDED) {
                    HideezIO.INSTANCE.connect(device)
                    activity?.supportFragmentManager
                            ?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, PairedSuccessFragment.newInstance(device.name))
                            ?.commitAllowingStateLoss()
                }
            }
        })

        devicesRecyclerView.adapter = deviceListAdapter
        devicesRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
            setDrawable(resources.getDrawable(R.drawable.divider))
        })

        refreshTextView.setOnClickListener {
            deviceListAdapter.clear()

            scanLeDevice()
        }

        val list = arguments?.getParcelableArrayList<BluetoothDevice>(LIST_DEVICES) ?: arrayListOf()
        deviceListAdapter.setDevices(list)

        if (list.size == 0) scanLeDevice()
    }
}

class DeviceListAdapter(private val callback: Callback) : RecyclerView.Adapter<DeviceListAdapter.ViewHolder>() {
    private val devices = arrayListOf<BluetoothDevice>()
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_device_ble, p0, false))
    }

    override fun getItemCount(): Int = devices.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.itemView.deviceNameTextView.text = devices[p1].name
        p0.itemView.setOnClickListener {
            callback.clickDevice(devices[p1])
        }
    }

    fun setDevices(list: ArrayList<BluetoothDevice>) {
        devices.clear()
        devices.addAll(list)

        notifyDataSetChanged()
    }

    fun addDevice(device: BluetoothDevice) {
        if (devices.find { it.address == device.address } == null && !device.name.isNullOrEmpty() &&
                device.name!!.contains("Hideez")) {
            devices.add(device)

            notifyDataSetChanged()
        }
    }

    fun clear() {
        devices.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface Callback {
        fun clickDevice(device: BluetoothDevice)
    }
}
