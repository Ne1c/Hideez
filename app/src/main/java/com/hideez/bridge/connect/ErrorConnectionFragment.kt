package com.hideez.bridge.connect


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import kotlinx.android.synthetic.main.fragment_error_connection.*


class ErrorConnectionFragment : Fragment() {
    enum class Error {
        BLUETOOTH_DISABLE, DONT_SEARCH_HIDEEZ, PAIRING_FAILED
    }

    companion object {
        fun newInstance(error: Error): ErrorConnectionFragment {
            return ErrorConnectionFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("error", error)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_error_connection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val error = arguments?.getSerializable("error") as Error

        when(error) {
            Error.BLUETOOTH_DISABLE -> {
                titleTextView.text = "Turn on Bluetooth, please"
                descTextView.text = "Hideez Wallet is connect to your smartphone\nthrough Bluetooth. So, turn it on and\ntry again."
                tryAgainButton.text = "Search again"
                imageView.setImageResource(R.drawable.ic_no_bt)
            }
            Error.DONT_SEARCH_HIDEEZ -> {
                titleTextView.text = "Sorry"
                descTextView.text = "We could not detect Hideez Wallet.\nBe sure that your smartphone is near of \nthe Hideez Wallet and try again."
                tryAgainButton.text = "Search again"
                imageView.setImageResource(R.drawable.ic_no_wallet)
            }
            Error.PAIRING_FAILED -> {
                titleTextView.text = "Hmm..."
                descTextView.text = "Something went wrong.\n Pairing is failed."
                tryAgainButton.text = "Try again"
                chooseAnotherDeviceButton.visibility = View.VISIBLE
                imageView.setImageResource(R.drawable.ic_smth_wrong)

                chooseAnotherDeviceButton.setOnClickListener {
                    activity?.supportFragmentManager
                            ?.beginTransaction()
                            ?.replace(R.id.fragmentContainer, DeviceListFragment.newInstance())
                            ?.commitAllowingStateLoss()
                }
            }
        }

        tryAgainButton.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.fragmentContainer, SearchFragment.newInstance())
                    ?.commitAllowingStateLoss()
        }
    }
}
