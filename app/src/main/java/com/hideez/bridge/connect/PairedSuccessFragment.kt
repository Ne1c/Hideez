package com.hideez.bridge.connect


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import com.hideez.bridge.setup.SetupActivity
import kotlinx.android.synthetic.main.fragment_paired_success.view.*

class PairedSuccessFragment : Fragment() {
    companion object {
        fun newInstance(name: String): PairedSuccessFragment {
            return PairedSuccessFragment().apply {
                arguments = Bundle().apply {
                    putString("name", name)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_paired_success, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val name = arguments!!.getString("name")
        view.descTextView.text = "$name\nwas successfully paired."

        view.postDelayed({
            startActivity(Intent(context, SetupActivity::class.java))
            activity?.finish()
        }, 3000)
    }
}
