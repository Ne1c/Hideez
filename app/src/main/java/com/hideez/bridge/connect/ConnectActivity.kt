
package com.hideez.bridge.connect

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hideez.bridge.R

class ConnectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connect)

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, SearchFragment.newInstance())
                .commitAllowingStateLoss()
    }
}
