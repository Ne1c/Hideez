package com.hideez.bridge.intro

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import com.hideez.bridge.R
import com.hideez.bridge.connect.ConnectActivity
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        viewPager.adapter = object : FragmentStatePagerAdapter(supportFragmentManager) {
            override fun getItem(page: Int): Fragment = IntroFragment.newInstance(page)

            override fun getCount(): Int = 3
        }
        dots_indicator.setViewPager(viewPager)
        nextButton.setOnClickListener {
            val nextItem = viewPager.currentItem + 1

            if (nextItem == viewPager?.adapter?.count) {
                startActivity(Intent(this, ConnectActivity::class.java))
                finish()
            } else {
                viewPager.setCurrentItem(nextItem, true)
            }
        }

        skipButton.setOnClickListener {
            startActivity(Intent(this, ConnectActivity::class.java))
            finish()
        }
    }
}
