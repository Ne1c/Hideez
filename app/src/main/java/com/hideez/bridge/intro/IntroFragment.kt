package com.hideez.bridge.intro

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hideez.bridge.R
import kotlinx.android.synthetic.main.fragment_intro.*

class IntroFragment : Fragment() {
    companion object {
        fun newInstance(page: Int): IntroFragment {
            val fragment = IntroFragment()
            fragment.arguments = Bundle().apply {
                putInt("page", page)
            }

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val page = arguments?.getInt("page")

        when(page) {
            0 -> {
                titleTextView.setText(R.string.intro_1)
                descTextView.setText(R.string.intro_11)
                imageView.setImageResource(R.drawable.intro_1)
            }
            1 -> {
                titleTextView.setText(R.string.intro_2)
                descTextView.setText(R.string.intro_22)
                imageView.setImageResource(R.drawable.intro_2)
            }
            2 -> {
                titleTextView.setText(R.string.intro_3)
                descTextView.setText(R.string.intro_33)
                imageView.setImageResource(R.drawable.intro_3)
            }
        }
    }
}
