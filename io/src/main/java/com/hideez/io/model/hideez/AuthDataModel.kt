package com.hideez.io.model.hideez

import com.hideez.io.command.ResponseCommand

class AuthDataModel(override val channelId: Int, payloadSize: Int) : DirectDataModel(channelId, ResponseCommand.MSG_AUTHREPLY, payloadSize) {
}