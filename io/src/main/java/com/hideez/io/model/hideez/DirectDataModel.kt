package com.hideez.io.model.hideez

import com.hideez.io.model.DataModel

/**
 * Class specific for hideez type messages
 *
 * @param msgId id of message can be either response or request or error
 * @see {@link com.hideez.io.command.Commands.RequestCommand}
 * @see {@link com.hideez.io.command.Commands.ResponseCommand}
 * @see {@link com.hideez.io.command.Commands.Error}
 */
open class DirectDataModel(override val channelId: Int, open val msgId: Int, payloadSize: Int) : DataModel(channelId, payloadSize) {
    override fun copy(): DirectDataModel {
        return DirectDataModel(this.channelId, msgId, this.payloadSize).also {
            this.preparePayload()
            it.appendToBuffer(this.payload)
            it.preparePayload()
        }
    }
}