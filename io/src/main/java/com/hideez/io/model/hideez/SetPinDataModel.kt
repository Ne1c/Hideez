package com.hideez.io.model.hideez

import com.hideez.io.command.ResponseCommand

class SetPinDataModel(override val channelId: Int, payloadSize: Int) : DirectDataModel(channelId, ResponseCommand.MSG_NEWPIN, payloadSize) {
    var stage: Stage = Stage.ENTER

    override fun preparePayload() {
        super.preparePayload()

        stage = Stage.values()[payload[0].toInt()] // 0 - enter, 1 - repeat, 2 - confirm
    }

    override fun copy(): SetPinDataModel {
        return SetPinDataModel(channelId, payloadSize).also {
            this.preparePayload()
            it.appendToBuffer(this.payload)
            it.preparePayload()

            it.stage = this.stage
        }
    }

    enum class Stage {
        ENTER, REPEAT, CONFIRM
    }
}