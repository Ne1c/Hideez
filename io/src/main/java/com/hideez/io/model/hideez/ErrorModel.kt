package com.hideez.io.model.hideez

class ErrorModel(override val channelId: Int,
                 override val msgId: Int) : DirectDataModel(channelId, msgId, 0)