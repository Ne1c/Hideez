package com.hideez.io.model.hideez

import com.hideez.io.command.ResponseCommand

class BackupWordModel(override val channelId: Int,
                      payloadSize: Int) : DirectDataModel(channelId, ResponseCommand.MSG_BACKUPWORD, payloadSize) {
    var number = 0
    var word = ""

    override fun preparePayload() {
        super.preparePayload()

        number = payload[0].toInt()
        word = String(payload.copyOfRange(1, payload.size).takeWhile { it.toInt() != 0x00 }.toByteArray())
    }

    override fun copy(): BackupWordModel {
        return BackupWordModel(channelId, payloadSize).also {
            this.preparePayload()
            it.appendToBuffer(this.payload)
            it.preparePayload()

            it.number = this.number
            it.word = this.word
        }
    }
}