package com.hideez.io.model

import java.nio.ByteBuffer

/**
 * Base data model for response, can be use as temporary buffer
 *
 * @param channelId channel id whom belongs this object
 * @param payloadSize size of payload inside object
 */
abstract class DataModel(open val channelId: Int, val payloadSize: Int) {
    private val _payload = ByteBuffer.allocate(32768)

    lateinit var payload: ByteArray

    private fun isReady(): Boolean = ::payload.isInitialized

    fun appendToBuffer(data: ByteArray) {
            _payload.put(data)
    }

    /**
     * Method which use for copy from temporary buffer to permanent
     */
    open fun preparePayload() {
        if (isReady()) return

        val size = _payload.position() + 1
        _payload.rewind()
        payload = ByteArray(size)
        _payload.get(payload)
    }

    /**
     * Like clone(), but copy. Should be override by child class
     */
    abstract fun copy(): DataModel
}