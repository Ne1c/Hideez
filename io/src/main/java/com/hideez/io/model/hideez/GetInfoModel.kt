package com.hideez.io.model.hideez

import com.hideez.io.command.ResponseCommand

class GetInfoModel(override val channelId: Int, payloadSize: Int) : DirectDataModel(channelId, ResponseCommand.MSG_INFO, payloadSize) {
    // DeviceInfo
    var initialized: Boolean = false
    var inSecure: Boolean = false

    var layoutVersion = 0
    var size = 0

    // FirmwareVersion
    var hwId = 0
    var fwMajor = 0
    var fwMinor = 0
    var fwRev = 0

    var blMajor = 0
    var blMinor = 0
    var blRev = 0

    var vendor = ""
    var product = ""
    var language = ""

    var nextBackupWord = 0
    var nextRecoverWord = 0

    override fun preparePayload() {
        super.preparePayload()

        layoutVersion = ((payload[1].toInt() shl 8) or payload[0].toInt())
        size = ((payload[3].toInt() shl 8) or payload[2].toInt())
        vendor = String(payload.copyOfRange(4, 20).takeWhile { it != 0x00.toByte() }.toByteArray())
        product = String(payload.copyOfRange(20, 36).takeWhile { it != 0x00.toByte() }.toByteArray())

        hwId = payload[36].toInt()
        fwMajor = payload[37].toInt()
        fwMinor = payload[38].toInt()
        fwRev = payload[39].toInt()

        blMajor = payload[41].toInt()
        blMinor = payload[42].toInt()
        blRev = payload[43].toInt()

        initialized = payload[80].toInt() == 1
        inSecure = payload[81].toInt() == 1

        nextBackupWord = payload[82].toInt()
        nextRecoverWord = payload[83].toInt()
    }
}