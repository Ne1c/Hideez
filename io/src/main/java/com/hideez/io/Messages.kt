package com.hideez.io

import android.util.Log
import com.google.protobuf.InvalidProtocolBufferException
import com.google.protobuf.Message
import com.hideez.io.command.*
import com.hideez.io.model.DataModel
import com.hideez.io.model.hideez.*
import com.hideez.io.protobuf.TrezorMessage
import com.hideez.io.util.Optional
import com.hideez.io.util.bytesToInt
import com.hideez.io.util.contentToHexString
import com.hideez.io.util.intSplit2Bytes
import java.nio.ByteBuffer
import java.util.*

class Messages {
    companion object {
        const val TAG = "Messages"

        private const val PACKAGE_SIZE = 20
        private const val DATA_SIZE = 16
    }

    /**
     * Byte buffer for Trezor message
     */
    private val readBufferTrezor = ByteBuffer.allocate(32768)

    private var messageSize = 0
    private var type: TrezorMessage.MessageType? = null

    /**
     * Temporary model for Hideez message
     */
    private var readDirectTempModel: DirectDataModel? = null

    // 0 or 1
    private var flipBit = -1

    /**
     * Create byte array of sending message according to Trezor protocol
     */
    fun createMessage(msg: Message): ArrayDeque<ByteArray> {
        var sendPackageNumber = 0
        val queue = ArrayDeque<ByteArray>()
        val data = ByteBuffer.allocate(32768)

        val msgSize = msg.serializedSize
        val msgName = msg.javaClass.simpleName
        val msgId = TrezorMessage.MessageType.valueOf("MessageType_$msgName").number

        changeFlipBit()

        data.put((msgId shr 8 and 0xFF).toByte())
        data.put((msgId and 0xFF).toByte())
        data.put((msgSize shr 24 and 0xFF).toByte())
        data.put((msgSize shr 16 and 0xFF).toByte())
        data.put((msgSize shr 8 and 0xFF).toByte())
        data.put((msgSize and 0xFF).toByte())
        data.put(msg.toByteArray())

        while (data.position() % PACKAGE_SIZE > 0) {
            data.put(0.toByte())
        }

        val chunks = (data.position() - 19) / DATA_SIZE

        data.rewind()

        // Set first package
        val firstPackage = ByteArray(PACKAGE_SIZE)
        data.get(firstPackage, 0, 19)
        queue.add(firstPackage)

        for (i in 0 until chunks) {
            val buffer = ByteArray(PACKAGE_SIZE)
            buffer[0] = 0x00

            val pkgId = createPkgId(Channel.TREZOR_MESSAGE_CHANNEL_ID, flipBit, 0, ++sendPackageNumber)

            buffer[1] = pkgId[0]
            buffer[2] = pkgId[1]

            data.get(buffer, 3, DATA_SIZE)

            queue.add(buffer)
        }

        queue.add(createHmac(Channel.TREZOR_MESSAGE_CHANNEL_ID, ++sendPackageNumber))

        return queue
    }

    /**
     * Create byte array of sending message according to Hideez protocol
     *
     * @param channelId
     * @param msgId
     * @param payload data which will send
     */
    fun createMessage(channelId: Int, msgId: Int, payload: ByteArray = ByteArray(0)): ArrayDeque<ByteArray> {
        var sendPackageNumber = 0
        val queue = ArrayDeque<ByteArray>()
        val data = ByteBuffer.allocate(32768)

        changeFlipBit()

        data.put(0x00)
        data.put(createPkgId(channelId, flipBit, 0, sendPackageNumber))
        data.put(((msgId shr 8) and 0xFF).toByte())
        data.put((msgId and 0xFF).toByte())
        data.put(((payload.size shr 8) and 0xFF).toByte())
        data.put((payload.size and 0xFF).toByte())
        data.put(payload)

        while (data.position() % PACKAGE_SIZE > 0) {
            data.put(0.toByte())
        }

        val chunks = (data.position() - 19) / DATA_SIZE

        data.rewind()

        // Set first package
        val firstPackage = ByteArray(PACKAGE_SIZE)
        data.get(firstPackage, 0, 19)
        queue.add(firstPackage)

        Log.d(Messages.TAG, "Send: \nchannel: $channelId" +
                "\nlast: 0\nflipBit:$flipBit\nmsgId: ${msgId.toString(16)}\nmsg_text: ${msgIdToString(msgId)}\n" +
                "data: ${firstPackage.contentToHexString()}")

        for (i in 0 until chunks) {
            val buffer = ByteArray(PACKAGE_SIZE)
            buffer[0] = 0x00

            val pkgId = createPkgId(Channel.DIRECT_MESSAGE_CHANNEL_ID, flipBit, 0, ++sendPackageNumber)

            buffer[1] = pkgId[0]
            buffer[2] = pkgId[1]

            data.get(buffer, 3, DATA_SIZE)

            queue.add(buffer)

            Log.d(Messages.TAG, "Send: \nchannel: $channelId" +
                    "\nlast: 0\nflipBit:$flipBit\nmsgId: ${msgId.toString(16)}\nmsg_text: ${msgIdToString(msgId)}\n" +
                    "data: ${buffer.contentToHexString()}")
        }

        // Create HMAC
        queue.add(createHmac(channelId, ++sendPackageNumber))

        return queue
    }

    /**
     * Create commit after received all messages
     * @param f is flipBit
     * @param l is last message
     * @param n is number of package within one message
     */
    fun createCommit(channelId: Int, f: Int, l: Int, n: Int): ByteArray {
        val array = ByteArray(PACKAGE_SIZE)

        array[0] = 0x00
        array[1] = COMMIT_MSG
        array[2] = ((channelId shl 5) or ((f and 1) shl 4) or ((l and 1) shl 3) or (n shr 8)).toByte()
        array[3] = (n and 0xFF).toByte()

        return array
    }

    /**
     * Create HMAC, now body is empty
     */
    private fun createHmac(channelId: Int, packageNumber: Int): ByteArray {
        val hmacBuffer = ByteBuffer.allocate(PACKAGE_SIZE)
        hmacBuffer.put(0x00)
        hmacBuffer.put(createPkgId(channelId, flipBit, 1, packageNumber))

        Log.d(Messages.TAG, "Send: \nchannel: $channelId" +
                "\nlast: 1\ndata: ${hmacBuffer.array().contentToHexString()}\nmsgText: HMAC")

        return hmacBuffer.array()
    }

    private fun changeFlipBit(): Int {
        flipBit = if (flipBit == -1 || flipBit == 1) 0 else 1
        return flipBit
    }

    private fun createPkgId(channelId: Int, flipBit: Int, lastMessage: Int, packageNumber: Int): ByteArray {
        val pkgId = (channelId shl 13) or (flipBit shl 12) or (lastMessage shl 11) or packageNumber
        return intSplit2Bytes(pkgId)
    }

    /**
     * Read get message or part of it
     *
     * @param commitCallback when call when all packages of message will be receive
     */
    fun readMessage(data: ByteArray,
                    commitCallback: (channelId: Int, flipBit: Int, last: Int, number: Int) -> Unit): Optional<DataModel> {
        when (data[1]) {
            COMMIT_MSG -> Log.d(TAG, "Received: commit: ${data.contentToHexString()}")
            PROGRESS_MSG -> Log.d(TAG, "Received: progress: ${data.contentToHexString()}")
            CANCEL_MSG -> Log.d(TAG, "Received: cancel: ${data.contentToHexString()}")
            else -> {
                val channelId = data[1].toInt() shr 5
                val flipBit = (data[1].toInt() shr 4) and 1
                val lastMessage = (data[1].toInt() shr 3) and 1
                val numberPackage = ((data[1].toInt() shl 8) or data[2].toInt()) and 0x7ff
                val msgId = bytesToInt(data[3], data[4])
                val payloadSize = bytesToInt(data[5], data[6])

//                this.flipBit = flipBit

                if (lastMessage != 1) {
                    val directMsgId = if (readDirectTempModel == null) msgId else readDirectTempModel!!.msgId

                    Log.d(TAG, "Received:\ndata:${data.contentToHexString()}\nchannel: $channelId" +
                            "\nlast: $lastMessage\nflipBit:$flipBit\nmsgId: ${directMsgId.toString(16)}" +
                            "\nmsgText: ${msgIdToString(directMsgId)}\nisError: ${isError(directMsgId)}")
                } else {
                    Log.d(TAG, "Received:\ndata:${data.contentToHexString()}\nchannel: $channelId" +
                            "\nmsgText: HMAC\nisError: ${isError(msgId)}")
                }

                // Check if it first package of message
                if (channelId == Channel.DIRECT_MESSAGE_CHANNEL_ID &&
                        cacheIsEmpty() && !isError(msgId) && numberPackage == 0) {
                    readDirectTempModel = createTempModelByMsgId(channelId, msgId, payloadSize)
                } else if (isError(msgId)) { // Check if it's error
                    resetCache()
                    return Optional.valueOf(ErrorModel(channelId, msgId))
                }

                // If it isn't last message write to temporary model, else send commit
                if (lastMessage != 1) {
                    writeToTempModel(channelId, numberPackage, data)
                } else {
                    commitCallback.invoke(channelId, flipBit, lastMessage, numberPackage + 1)

                    if (channelId == Channel.DIRECT_MESSAGE_CHANNEL_ID) {
                        val copyDataModel = readDirectTempModel?.copy()
                        resetCache()

                        return Optional.valueOf(copyDataModel)
                    }
                }

                return Optional.empty()
            }
        }

        return Optional.empty()
    }

    private fun readTrezorMessage(array: ByteArray): Optional<Message> {
        if (messageSize == 0) {
            messageSize = ((array[3].toInt() and 0xFF) shl 24) +
                    ((array[4].toInt() and 0xFF) shl 16) +
                    ((array[5].toInt() and 0xFF) shl 8) +
                    (array[6].toInt() and 0xFF)

            type = TrezorMessage.MessageType.valueOf(((array[1].toInt() and 0xFF) shl 8) + (array[2].toInt() and 0xFF))
            readBufferTrezor.put(array, 7, array.size - 7)
        } else {
            readBufferTrezor.put(array, 2, array.size - 2)
        }

        if (readBufferTrezor.position() >= messageSize) {
            val value = parseTrezorMessageFromBytes(type!!, Arrays.copyOfRange(readBufferTrezor.array(), 0, messageSize))!!

            return Optional.valueOf(value)

        }

        return Optional.empty()
    }

    private fun parseTrezorMessageFromBytes(type: TrezorMessage.MessageType, data: ByteArray): Message? {
        var msg: Message? = null
        try {
            if (type.number == TrezorMessage.MessageType.MessageType_Success_VALUE) {
                msg = TrezorMessage.Success.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_Failure_VALUE) {
                msg = TrezorMessage.Failure.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_Entropy_VALUE) {
                msg = TrezorMessage.Entropy.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_PublicKey_VALUE) {
                msg = TrezorMessage.PublicKey.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_Features_VALUE) {
                msg = TrezorMessage.Features.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_PinMatrixRequest_VALUE) {
                msg = TrezorMessage.PinMatrixRequest.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_TxRequest_VALUE) {
                msg = TrezorMessage.TxRequest.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_ButtonRequest_VALUE) {
                msg = TrezorMessage.ButtonRequest.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_Address_VALUE) {
                msg = TrezorMessage.Address.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_EntropyRequest_VALUE) {
                msg = TrezorMessage.EntropyRequest.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_MessageSignature_VALUE) {
                msg = TrezorMessage.MessageSignature.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_PassphraseRequest_VALUE) {
                msg = TrezorMessage.PassphraseRequest.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_TxSize_VALUE) {
                msg = TrezorMessage.TxSize.parseFrom(data)
            }
            if (type.number == TrezorMessage.MessageType.MessageType_WordRequest_VALUE) {
                msg = TrezorMessage.WordRequest.parseFrom(data)
            }
        } catch (e: InvalidProtocolBufferException) {
            return null
        }

        return msg
    }

    /**
     * Method for check if it's error msgId
     */
    fun isError(msgId: Int): Boolean {
        when (msgId) {
            Errors.ERR_UNKNOWN_MESSAGE -> return true
            Errors.ERR_NOT_INITIALIZED -> return true
            Errors.ERR_ALREADY_INITIALIZED -> return true
            Errors.ERR_ENCRYPTION_REQ -> return true
            Errors.ERR_DEVICE_LOCKED -> return true
            Errors.ERR_UNAUTHORIZED -> return true
            Errors.ERR_DEVICE_BUSY -> return true
            Errors.ERR_UNSUPPORTED -> return true
            Errors.ERR_MESSAGE_TOO_LONG -> return true
            Errors.ERR_UNLOCK_KEY_USED -> return true
            Errors.ERR_PASSPHRASE_UNKNOWN -> return true
            Errors.ERR_INVALID_PARAMETER -> return true
            Errors.ERR_OPERATION_CANCELED -> return true
            Errors.ERR_HARDWARE_FAULT -> return true
            Errors.ERR_WRONG_PROFILE -> return true
            Errors.ERR_KEY_USED -> return true
            Errors.ERR_KEY_WRONG -> return true
            Errors.ERR_BAD_RECOVERY_DATA -> return true
            Errors.ERR_AUTHORIZATION_FAILED -> return true
            Errors.ERR_CHANNEL_BUSY -> return true
            else -> return false
        }
    }

    /**
     * Clear temporary model
     */
    private fun resetCache() {
        readDirectTempModel = null
    }

    private fun writeToTempModel(channelId: Int, numberPackage: Int, data: ByteArray) {
        if (channelId == Channel.DIRECT_MESSAGE_CHANNEL_ID) {
            if (numberPackage == 0) {
                readDirectTempModel?.appendToBuffer(data.copyOfRange(7, data.size - 1))
            } else {
                readDirectTempModel?.appendToBuffer(data.copyOfRange(3, data.size - 1))
            }
        } else if (channelId == Channel.TREZOR_MESSAGE_CHANNEL_ID) {
            readTrezorMessage(data.copyOfRange(4, data.size))
        }
    }

    /**
     * Create specific temporary model if model for type of msgId exist
     */
    private fun createTempModelByMsgId(channelId: Int, msgId: Int, payloadSize: Int): DirectDataModel {
        return when (msgId) {
            ResponseCommand.MSG_NEWPIN -> {
                SetPinDataModel(channelId, payloadSize)
            }
            ResponseCommand.MSG_INFO -> {
                GetInfoModel(channelId, payloadSize)
            }
            ResponseCommand.MSG_AUTHREPLY -> {
                AuthDataModel(channelId, payloadSize)
            }
            ResponseCommand.MSG_BACKUPWORD -> {
                BackupWordModel(channelId, payloadSize)
            }
            else -> {
                DirectDataModel(channelId, msgId, payloadSize)
            }
        }
    }

    /**
     * Check temporary model is empty
     */
    private fun cacheIsEmpty(): Boolean = readDirectTempModel == null
}