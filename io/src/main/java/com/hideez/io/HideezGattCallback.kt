package com.hideez.io

import android.bluetooth.BluetoothDevice
import no.nordicsemi.android.ble.BleManagerCallbacks

/**
 * Global listener of bluetooth events
 */
class HideezGattCallback(private val callbacks: ArrayList<BleManagerCallbacks>) : AbsHideezGattCallback() {
    override fun onDeviceDisconnecting(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceDisconnecting(device)
        }
    }

    override fun onDeviceDisconnected(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceDisconnected(device)
        }
    }

    override fun onLinklossOccur(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onLinklossOccur(device)
        }
    }

    override fun onDeviceConnected(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceConnected(device)
        }
    }

    override fun onDeviceNotSupported(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceNotSupported(device)
        }
    }

    override fun onServicesDiscovered(device: BluetoothDevice?, optionalServicesFound: Boolean) {
        callbacks.forEach {
            it.onServicesDiscovered(device, optionalServicesFound)
        }
    }

    override fun onBondingRequired(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onBondingRequired(device)
        }
    }

    override fun onBatteryValueReceived(device: BluetoothDevice?, value: Int) {
        callbacks.forEach {
            it.onBatteryValueReceived(device, value)
        }
    }

    override fun onBonded(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onBonded(device)
        }
    }

    override fun shouldEnableBatteryLevelNotifications(device: BluetoothDevice?): Boolean {
        callbacks.forEach {
            it.shouldEnableBatteryLevelNotifications(device)
        }
        return false
    }

    override fun onDeviceReady(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceReady(device)
        }
    }

    override fun onError(device: BluetoothDevice?, message: String?, errorCode: Int) {
        callbacks.forEach {
            it.onError(device, message, errorCode)
        }
    }

    override fun onDeviceConnecting(device: BluetoothDevice?) {
        callbacks.forEach {
            it.onDeviceConnecting(device)
        }
    }
}