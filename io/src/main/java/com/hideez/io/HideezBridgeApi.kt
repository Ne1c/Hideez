package com.hideez.io

import com.hideez.io.command.Channel
import com.hideez.io.command.RequestCommand
import com.hideez.io.crypto.Secp256k1
import com.hideez.io.util.generate16ByteRandomNumber

/**
 * Class that represent Hideez api in more understandable view
 * This class users should be use
 *
 * @param messages class that prepare data for sends on device
 * @param hideezBleIO class that sends data on device
 */
class HideezBridgeApi(private val messages: Messages,
                      private val hideezBleIO: HideezIO) {
    fun auth() {
        val privateKey = Secp256k1.generatePrivateKey()
        val publicKey = Secp256k1.getPublicKey(privateKey)
        val nonceh = generate16ByteRandomNumber()

        val authPayload = ByteArray(publicKey.size + nonceh.size + 1)
        authPayload[0] = Channel.DIRECT_MESSAGE_CHANNEL_ID.toByte()
        System.arraycopy(publicKey, 0, authPayload, 1, publicKey.size)
        System.arraycopy(nonceh, 0, authPayload, publicKey.size + 1, nonceh.size)

        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_AUTH,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_AUTH, authPayload))
    }

    fun wipe() {
        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_WIPE,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_WIPE))
    }

    fun requestDeviceInfo() {
        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_GETINFO,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_GETINFO))
    }

    fun init() {
        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_INITIALIZE,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_INITIALIZE))
    }

    // Call after either init or backup or recovert
    fun activate() {
        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_ACTIVATE,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_ACTIVATE))
    }

    fun backup() {
        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_BACKUP,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_BACKUP))
    }

    fun recover(numberWord: Int, word: String) {
        val data = ByteArray(1 + word.length)
        data[0] = numberWord.toByte()

        val wordByteArr = word.toByteArray()
        for (i in 1 until data.size) data[i] = wordByteArr[i - 1]

        hideezBleIO.sendMessageToHideez(RequestCommand.MSG_RECOVER,
                messages.createMessage(Channel.DIRECT_MESSAGE_CHANNEL_ID, RequestCommand.MSG_RECOVER, data))
    }
}