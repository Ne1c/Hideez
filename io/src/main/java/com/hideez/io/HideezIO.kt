package com.hideez.io

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.google.protobuf.Message
import com.hideez.io.command.*
import com.hideez.io.model.DataModel
import com.hideez.io.model.hideez.BackupWordModel
import com.hideez.io.model.hideez.DirectDataModel
import com.hideez.io.model.hideez.ErrorModel
import com.hideez.io.model.hideez.SetPinDataModel
import com.hideez.io.util.Optional
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.BleManagerCallbacks
import no.nordicsemi.android.ble.Request
import java.util.*

class HideezIO private constructor(context: Context) : BleManager<HideezGattCallback>(context) {
    companion object {
        lateinit var INSTANCE: HideezIO

        fun init(context: Context) {
            INSTANCE = HideezIO(context)
        }
    }

    private val TAG = "HideezBle"

    private val uiHandler = Handler(Looper.getMainLooper())

    private val serviceUUID = UUID.fromString("49ac0001-0f4a-ca95-0849-a6670829557f")!!
    private val characteristicUUID = UUID.fromString("49ac0009-0f4a-ca95-0849-a6670829557f")
    private var characteristicIO: BluetoothGattCharacteristic? = null

    /**
     * Creator and parser messages
     */
    private val messages = Messages()

    /**
     * General queue for send messages to device
     */
    private val sendMessageQueue = ArrayDeque<ByteArray>()
    /**
     * History of commands that contain msgIds, priority using for check what commands were sent when received MSG_SUCCESS
     */
    private val commandHistory = Stack<Int>()
    private val commandListeners = arrayListOf<CommandListener>()

    private val gattCallbacks = arrayListOf<BleManagerCallbacks>()

    private var device: BluetoothDevice? = null
    val bridgeApi: HideezBridgeApi = HideezBridgeApi(messages, this)

    init {
        setGattCallbacks(HideezGattCallback(gattCallbacks))
    }

    private val gattCallback = object : BleManagerGattCallback() {
        override fun onDeviceDisconnected() {
        }

        override fun isRequiredServiceSupported(gatt: BluetoothGatt?): Boolean {
            characteristicIO = gatt?.getService(serviceUUID)
                    ?.getCharacteristic(characteristicUUID)
            return characteristicIO != null
        }

        override fun initGatt(gatt: BluetoothGatt?): Deque<Request> {
            val list = LinkedList<Request>()

            list.push(Request.newEnableNotificationsRequest(characteristicIO))
            return list
        }

        override fun onDeviceReady() {
            super.onDeviceReady()

            bridgeApi.auth()
        }

        override fun onCharacteristicNotified(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            val data = readMessageAndSendCommit(characteristic!!.value)

            if (!data.isEmpty() && data.getValue().channelId == Channel.DIRECT_MESSAGE_CHANNEL_ID) {
                val value = data.getValue() as DirectDataModel
                when (value) {
                    is SetPinDataModel -> {
                        commandListeners.forEach {
                            uiHandler.post { it.setPin(value.stage) }
                        }
                    }
                    is BackupWordModel -> {
                        commandListeners.forEach {
                            uiHandler.post { it.backupWord(value.number, value.word) }
                        }
                    }
                    is ErrorModel -> {
                        if (BuildConfig.DEBUG) {
                            uiHandler.post {
                                Toast.makeText(context, msgIdToString(value.msgId), Toast.LENGTH_LONG).show()
                            }
                        }

                        commandListeners.forEach {
                            uiHandler.post { it.error(value) }
                        }
                    }
                    else -> {
                        when {
                            value.msgId == ResponseCommand.MSG_AUTHREPLY -> {
                                commandListeners.forEach {
                                    uiHandler.post { it.auth() }
                                }
                            }
                            value.msgId == ResponseCommand.MSG_SUCCESS -> handleSuccess()
                            value.msgId == ResponseCommand.MSG_QUERYPIN -> commandListeners.forEach {
                                uiHandler.post { it.queryPin() }
                            }
                        }
                    }
                }

                commandHistory.push(value.msgId)
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            sendData()
        }
    }

    override fun connect(device: BluetoothDevice) {
        this.device = device
        super.connect(device)
    }

    fun addListener(listener: CommandListener) {
        commandListeners.add(listener)
    }

    fun removeListener(listener: CommandListener) {
        commandListeners.remove(listener)
    }

    fun addGattCallback(callback: BleManagerCallbacks) {
        gattCallbacks.add(callback)
    }

    fun removeGattCallback(callback: BleManagerCallbacks) {
        gattCallbacks.remove(callback)
    }

    override fun shouldAutoConnect(): Boolean = false

    /**
     * Read message and send commit if it was last message
     */
    private fun readMessageAndSendCommit(data: ByteArray): Optional<DataModel> {
        return messages.readMessage(data) { channelId, f, l, n ->
            sendMessageQueue.add(messages.createCommit(channelId, f, l, n))

            sendData()
        }
    }

    /**
     * Added data byte array to general queue and then send
     */
    fun sendMessageToHideez(msgId: Int, data: ArrayDeque<ByteArray>) {
        sendMessageQueue.addAll(data)
        commandHistory.push(msgId)
        sendData()
    }

    fun sendMessageToTrezor(message: Message) {
        sendMessageQueue.addAll(messages.createMessage(message))

        sendData()
    }

    /**
     * Send data from general queue
     */
    internal fun sendData() {
        if (sendMessageQueue.isEmpty()) return

        val array = sendMessageQueue.pop()!!

        enqueue(Request.newWriteRequest(characteristicIO, array))
    }

    override fun getGattCallback(): BleManagerGattCallback = gattCallback

    /**
     * When from device comes MSG_SUCCESS
     */
    fun handleSuccess() {
        when (commandHistory.peek()) {
            ResponseCommand.MSG_NEWPIN -> {
                commandListeners.forEach {
                    uiHandler.post { it.pinSet() }
                }
            }
            ResponseCommand.MSG_BACKUPWORD -> {
                commandListeners.forEach {
                    uiHandler.post { it.backupDone() }
                }
            }
            RequestCommand.MSG_RECOVER -> {
                commandListeners.forEach {
                    uiHandler.post { it.recoverWordWasSend() }
                }
            }
        }
    }
}