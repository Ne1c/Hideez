package com.hideez.io.command


const val COMMIT_MSG: Byte = 0x10
const val PROGRESS_MSG: Byte = 0x11
const val CANCEL_MSG: Byte = 0x12

/**
 * Available channels for device
 */
object Channel {
    const val DIRECT_MESSAGE_CHANNEL_ID = 1
    const val TREZOR_MESSAGE_CHANNEL_ID = 2
}

/**
 * Possible requests commands
 */
object RequestCommand {
    const val MSG_AUTH = 0xA000
    const val MSG_CLOSECHANNEL = 0xA003
    const val MSG_GETINFO = 0xA004
    const val MSG_CONFIGURE = 0xA006
    const val MSG_INITIALIZE = 0xA008
    const val MSG_BACKUP = 0xA009
    const val MSG_ACTIVATE = 0xA00A
    const val MSG_CHANGEPIN = 0xA00B
    const val MSG_RECOVER = 0xA00C
    const val MSG_UNLOCK = 0xA00E
    const val MSG_WIPE = 0xA00F
    const val MSG_SELECT = 0xA010
    const val MSG_PASSPHRASE = 0xA011
    const val MSG_PING = 0xA01E
    const val MSG_BOOT = 0xA0FE
}

/**
 * Possible responses
 */
object ResponseCommand {
    const val MSG_AUTHREPLY = 0xA001
    const val MSG_SUCCESS = 0xA002
    const val MSG_INFO = 0xA005
    const val MSG_PING_REPLY = 0xA01F
    const val MSG_QUERYPIN = 0xA064
    const val MSG_NEWPIN = 0xA065
    const val MSG_CONFIRM = 0xA068
    const val MSG_BACKUPWORD = 0xA06A
}

/**
 * Errors
 */
object Errors {
    const val ERR_UNKNOWN_MESSAGE = 0xE101
    const val ERR_NOT_INITIALIZED = 0xE102
    const val ERR_ALREADY_INITIALIZED = 0xE103
    const val ERR_ENCRYPTION_REQ = 0xE104
    const val ERR_DEVICE_LOCKED = 0xE105
    const val ERR_UNAUTHORIZED = 0xE106
    const val ERR_DEVICE_BUSY = 0xE108
    const val ERR_UNSUPPORTED = 0xE109
    const val ERR_MESSAGE_TOO_LONG = 0xE10A
    const val ERR_UNLOCK_KEY_USED = 0xE10B
    const val ERR_PASSPHRASE_UNKNOWN = 0xE10C
    const val ERR_INVALID_PARAMETER = 0xE10D
    const val ERR_OPERATION_CANCELED = 0xE10E
    const val ERR_HARDWARE_FAULT = 0xE10F
    const val ERR_WRONG_PROFILE = 0xE110
    const val ERR_KEY_USED = 0xE111
    const val ERR_KEY_WRONG = 0xE112
    const val ERR_BAD_RECOVERY_DATA = 0xE113
    const val ERR_AUTHORIZATION_FAILED = 0xE114
    const val ERR_CHANNEL_BUSY = 0xE115
}

fun msgIdToString(msgId: Int): String {
    return when(msgId) {
        RequestCommand.MSG_AUTH -> "auth"
        RequestCommand.MSG_INITIALIZE -> "init"
        RequestCommand.MSG_BACKUP -> "backup"
        RequestCommand.MSG_ACTIVATE -> "activate"
        RequestCommand.MSG_BOOT -> "boot"
        RequestCommand.MSG_CHANGEPIN -> "change_pin"
        RequestCommand.MSG_CLOSECHANNEL -> "close_channel"
        RequestCommand.MSG_CONFIGURE -> "configure"
        RequestCommand.MSG_GETINFO -> "get_info"
        RequestCommand.MSG_PASSPHRASE -> "passphrase"
        RequestCommand.MSG_PING -> "ping"
        RequestCommand.MSG_RECOVER -> "recover"
        RequestCommand.MSG_SELECT -> "select"
        RequestCommand.MSG_UNLOCK-> "unlock"
        RequestCommand.MSG_WIPE-> "wipe"
        ResponseCommand.MSG_AUTHREPLY -> "auth_reply"
        ResponseCommand.MSG_SUCCESS -> "success"
        ResponseCommand.MSG_INFO -> "info"
        ResponseCommand.MSG_PING_REPLY -> "ping_reply"
        ResponseCommand.MSG_QUERYPIN -> "querypin"
        ResponseCommand.MSG_NEWPIN -> "newpin"
        ResponseCommand.MSG_CONFIRM -> "confirm"
        ResponseCommand.MSG_BACKUPWORD -> "backupword"
        Errors.ERR_UNKNOWN_MESSAGE -> "err_unknown_msg"
        Errors.ERR_NOT_INITIALIZED -> "err_not_init"
        Errors.ERR_ALREADY_INITIALIZED -> "err_already_init"
        Errors.ERR_ENCRYPTION_REQ -> "err_encript_req"
        Errors.ERR_DEVICE_LOCKED -> "err_device_locked"
        Errors.ERR_UNAUTHORIZED -> "err_unauth"
        Errors.ERR_DEVICE_BUSY -> "err_device_busy"
        Errors.ERR_UNSUPPORTED -> "err_unsupported"
        Errors.ERR_MESSAGE_TOO_LONG -> "err_message_too_long"
        Errors.ERR_UNLOCK_KEY_USED -> "err_unlock_key_used"
        Errors.ERR_PASSPHRASE_UNKNOWN -> "err_passphrase_uknown"
        Errors.ERR_INVALID_PARAMETER -> "err_invalid_param"
        Errors.ERR_OPERATION_CANCELED -> "err_operation_canceled"
        Errors.ERR_HARDWARE_FAULT -> "err_hardware_fault"
        Errors.ERR_WRONG_PROFILE -> "err_wrong_profile"
        Errors.ERR_KEY_USED -> "err_key_used"
        Errors.ERR_KEY_WRONG -> "err_key_wrong"
        Errors.ERR_BAD_RECOVERY_DATA -> "err_bad_recovery_data"
        Errors.ERR_AUTHORIZATION_FAILED -> "err_auth_failed"
        Errors.ERR_CHANNEL_BUSY -> "err_channel_busy"
        else -> "0 - unknown"
    }
}