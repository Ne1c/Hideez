package com.hideez.io.command

import com.hideez.io.model.hideez.ErrorModel
import com.hideez.io.model.hideez.SetPinDataModel

/**
 * Listener of received responses wrapped in understandable methods
 */
abstract class CommandListener {
    open fun setPin(stage: SetPinDataModel.Stage) {}

    open fun pinSet() {}

    open fun queryPin() {}

    open fun auth() {}

    open fun backupWord(number: Int, word: String) {}

    open fun backupDone() {}

    open fun recoverWordWasSend() {}

    open fun error(value: ErrorModel) {}
}