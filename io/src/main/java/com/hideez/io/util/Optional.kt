package com.hideez.io.util

class Optional<T> {
    private val value: T?

    private constructor(value: T?) {
        this.value = value
    }

    fun isEmpty() = value == null

    fun isPresent() = value != null

    fun getValue() = value!!

    companion object {
        fun <T> empty(): Optional<T> {
            return Optional(null)
        }

        fun <T> valueOf(value: T?): Optional<T> {
            return Optional(value)
        }
    }
}