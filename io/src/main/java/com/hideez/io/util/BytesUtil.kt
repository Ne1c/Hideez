package com.hideez.io.util

import java.security.SecureRandom

internal fun intSplit2Bytes(value: Int): ByteArray {
    val mask = 0xFF
    val byteArray = ByteArray(2)

    byteArray[1] = (value and mask).toByte()
    byteArray[0] = value.shr(8).toByte()

    return byteArray
}

internal fun bytesToInt(first: Byte, second: Byte): Int {
    return 0x00 shl 24 or 0x00 shl 16 or (first.toInt() and 0xff) shl 8 or (second.toInt() and 0xff)
}

internal fun ByteArray.contentToHexString(): String {
    var contentHex = ""

    for (i in 0 until size) {
        contentHex += String.format("%02X", this[i]) + "-"
    }

    contentHex = contentHex.removeRange(contentHex.length - 1, contentHex.length)

    return contentHex
}

internal fun generate16ByteRandomNumber(): ByteArray {
    val sr = SecureRandom()
    val rndBytes = ByteArray(16)
    sr.nextBytes(rndBytes)

    return rndBytes
}
