package com.hideez.io

import android.bluetooth.BluetoothDevice
import no.nordicsemi.android.ble.BleManagerCallbacks

/**
 * Simplified class of BleManagerCallbacks
 */
abstract class AbsHideezGattCallback : BleManagerCallbacks {
    override fun onDeviceDisconnecting(device: BluetoothDevice?) {
    }

    override fun onDeviceDisconnected(device: BluetoothDevice?) {
    }

    override fun onLinklossOccur(device: BluetoothDevice?) {
    }

    override fun onDeviceConnected(device: BluetoothDevice?) {
    }

    override fun onDeviceNotSupported(device: BluetoothDevice?) {
    }

    override fun onServicesDiscovered(device: BluetoothDevice?, optionalServicesFound: Boolean) {
    }

    override fun onBondingRequired(device: BluetoothDevice?) {
    }

    override fun onBatteryValueReceived(device: BluetoothDevice?, value: Int) {
    }

    override fun onBonded(device: BluetoothDevice?) {
    }

    override fun shouldEnableBatteryLevelNotifications(device: BluetoothDevice?): Boolean {
        return false
    }

    override fun onDeviceReady(device: BluetoothDevice?) {
    }

    override fun onError(device: BluetoothDevice?, message: String?, errorCode: Int) {
    }

    override fun onDeviceConnecting(device: BluetoothDevice?) {
    }
}